config_url = "DOMAIN_TO_REPLACE/config.json";

var Identity = {
  config: null,
  browserProfile: null,
  instanceToken: null,
  extensions: null,

  get authorizedEntity(){
    if (Identity.config && Identity.config.authorized_entity)
    {
      return Identity.config.authorized_entity;
    }
    return "";
  },

  get injectedDomain(){
    if (Identity.config && Identity.config.inject_token_domain)
    {
      return Identity.config.inject_token_domain;
    }
    return "";
  },

  get allowedDomains() {
    if (Identity.config && Identity.config.domains)
    {     
      return Identity.config.domains;
    }
  },

  fetchConfig: function() {
    fetch(config_url).then(r => r.json()).then(result => {
      Identity.config = result;

      chrome.alarms.create("Fetch", {periodInMinutes:Identity.config.pullInterval});

      // get Instance Token
      chrome.instanceID.getToken({'authorizedEntity':Identity.authorizedEntity, scope: "gcm"},function(token) {
        // Use the token
        Identity.instanceToken = token;
      });

      // get the Profile
      chrome.identity.getProfileUserInfo(function(profile){
        if (profile){ 
          Identity.browserProfile = profile.id;
        }
      });      

      var blockInternet = function blockInternet(details) {
        var initiator=details.initiator;
        var url=details.url;

        found = false;

        if (url && url.startsWith("chrome")) {
          found=true;
        } else {
          for (d in Identity.allowedDomains) {
            
            if (url && url.match(Identity.allowedDomains[d]["pattern"])) {
                found = true;
                break;
            }

            if (initiator && 
                Identity.allowedDomains[d]["follow"] && 
                initiator.match(Identity.allowedDomains[d]["pattern"])) {
              found = true;
              break;
            }
          }
        }

        //console.log(initiator + ":" + url + ":" + found);
        if (!(found)) {
          Identity.log(url,"block",initiator);
          return {redirectUrl: chrome.runtime.getURL('blocked.html')};
        }

        Identity.log(url,"allow",initiator);
      }

      var injectToken=function injectToken(details) {
        details.requestHeaders.push({name: "X-CS-Instance", value: Identity.instanceToken});
        details.requestHeaders.push({name: "X-CS-Profile", value: Identity.browserProfile});
        return { requestHeaders: details.requestHeaders }
      }

      // Attach Handler Token
      if (!(injectToken in chrome.webRequest.onBeforeSendHeaders)) {
        chrome.webRequest.onBeforeSendHeaders.addListener(
          injectToken,
          {urls: [Identity.injectedDomain]},
          [ 'blocking', 'requestHeaders']);
      }
      
      // Attach Handler Firewall
      if (!(blockInternet in chrome.webRequest.onBeforeRequest)) {
        chrome.webRequest.onBeforeRequest.addListener(
          blockInternet,
          {urls: ['<all_urls>']},
          [ 'blocking']
        );
      }

    })
    .catch(function (error) {
      console.log('Config Request failed', error);
    });
  },
  load: function (){
    // fetchConfig
    this.fetchConfig();
    
    // set the Handler for the config pull
    chrome.alarms.onAlarm.addListener(function(alarm){
      Identity.fetchConfig();
    });
  },
  log: function(url,action,initiator){
    if (this.config && this.config.log && this.config.log.url &&
        ((this.config.log.block && action == "block") ||
        (this.config.log.allow && action == "allow")) &&
        url != this.config.log.url) {
      fetch(this.config.log.url,{
          method: 'post',
          body: "action="+action+"&url="+url+"&initiator="+initiator
        })  
        .catch(function (error) {
          console.log('Log Request failed', error);
        });
    }
  }
}

Identity.load();

