# Chrome shield 

Efectivly separate corporate and personal browser profiles

## Features

* **Authentication**: Our extension injects a token that an enrolled Chrome has available and appends it into a request header
This token is then validated by the nginx reverse proxy using a module to validate oAuth with the Identity Provider (in this case Google)

* **Protection**: The extension needs permission on all sites, by using the chrome.webRequest power it can grant, deny or modify the navigation on any given url.

* **Enrollment**: Using the GSuite Chrome's enrollment, we are able to configure only this browser's profile and make it compliant with our security policies, such as:
    * Password Manager
    * Proxy settings
    * Force app and extensions
    * Ephemeral Mode

## How does it works? 

The whole protection flow relies on the following "transitive" property

* To be able to access our protected systems a token must be present in the headers of each request
*then*
* To be able to append that token to each request user must install our extension
*then*
* To be able to install our extension you must enroll your browser to the corporate’s GSuite account 

---

## Getting Started

These instructions will get you a copy of the project up and running on your local machine. 

### Prerequisites

This is intended to run along side with GSuite and a nginx as a hub for access your web resources in a centralized fashion

* Administrative rights in Google GSuite
    * Manage OUs in the directory
    * Create projects and IAM credentials
    * Create firebase projects for messaging

* Network access for Nginx 
    * This service will securely proxy requests, so access from this docker to the end service is required
    * As this docker uses Google's Firebase Cloud Messaging to validate the client authenticity, so access to that service is required (internet)

### Pre-Configuring it

* **Getting your project in Google Developers Console**

Go to console.developers.google.com and create a project as following
![Google API Project](screenshots/google_project.jpg?raw=true "Google API Project")

* **Getting your firebase project for cloud messaging**
![Fibebase Project](screenshots/firebase_config.jpg?raw=true "Fibebase Project")

* **Configure your Organizational Unit in GSuite**
Go to Admin Console -> Devices -> Chrome management -> User & Browser Settings and configure your settings as desired

![GSuite OU Config](screenshots/google_ou_config.jpg?raw=true "OU Config")

![GSuite Extensions Config](screenshots/google_extension_force.jpg?raw=true "GSuite Extensions Config")


### Tunning the config.json

The config is small but powerfull, next you'll find a file similar to:

```
{
    "authorized_entity": "[PROJECT ID]",
    "inject_token_domain": "*://[SUBDOMAIN/Wildcard].[DOMAIN]/",
    "domains": [ "[ALLOWED_URL_REGEX]", "[ANOTHER_ALLOWED_URL_REGEX]" ]
}
```

Where:

* **authorized_entity**: is the project id created obtained in the Google Developers Console
* **inject_token_domain**: is the domain to which this extension will inject the authentication token to
* **domains**: its a regex array that allows you to control which sites will be reachable using this controlled browser.
NOTE: for usability sake, these domains are included as allowed referer as well. 


### Installing


**NGINX**
To Build the nginx docker image with LUA support issue the following commands

```
docker build -f /[project_path]/docker/dockerfile . -t openresty_extension

# Place correct values on the config.json 
# Configure authorizationCredential with the **server key** obtained in the firebase setup

docker run --name nginx_test -v [project_path]/nginx/:/etc/nginx/conf.d:ro -d openresty_extension:latest
```

**EXTENSION**
To pack and upload the extension run the following
```
sed -i 's/DOMAIN_TO_REPLACE/[http:\/\/place.yourdomain.com]/g' [project_path]/chrome_extension/manifest.json
sed -i 's/DOMAIN_TO_REPLACE/[http:\/\/place.yourdomain.com]/g' [project_path]/chrome_extension/background.js 
zip /tmp/chrome_extension.zip [project_path]/chrome_extension/* 
```

Now go to the Chrome Developer WebStore and create a new item by uploading this zip file
There you can set its visibility to only allowed company users or publicly available.

---

## Built With

* [openresty](https://openresty.org/en/) - OpenResty® is a dynamic web platform based on NGINX and LuaJIT

* [gsuite](https://gsuite.google.com/) - G Suite: Collaboration & Productivity Apps for Business

* [firebase](https://firebase.google.com) - Firebase is Google's mobile platform that helps you quickly develop high-quality apps and grow your business

## Acknowledgments

* nginx site for oAuth authentication:
https://www.nginx.com/blog/validating-oauth-2-0-access-tokens-nginx/
